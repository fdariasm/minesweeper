package com.gmail.at.nowxue.minesweeper;

import java.util.Set;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ZoomControls;

import com.actionbarsherlock.app.SherlockFragment;
import com.gmail.at.nowxue.minesweeper.model.DbHelper;
import com.gmail.at.nowxue.minesweeper.model.GameModel;
import com.gmail.at.nowxue.minesweeper.model.TextDrawable;
/*
 * The most important class in the application. It handles 
 * all the game logic.
 */
public class GameBoard extends SherlockFragment 
					implements OnClickListener, OnLongClickListener, 
					OnSharedPreferenceChangeListener, 
					DbHelper.OnGamestateListener{
	
	//Used to scale the slots in the board, images and text included.
	private static final double DEFAULT_SCALE = 1.4;
	private static final String PLAYER_DIALOG_FRAGMENT = "PlayerDialogFragment";
	//Key for storing values in the preferences
	private static final String SCALE_PREFERENCE = "scale_preference";
	private static final String FLAGS_PREFERENCE = "flags_preference";
	
	//The maximum scale allowed
	private static final double UPPER_LIMIT = 1.7;
	//The minimum scale allowed
	private static final double BOTTOM_LIMIT = 1.1;
	//Step for increase the scale each time a zoom happens 
	private static final double INCREMENT = 0.4;
	//Defines the color for the shadowing in the text both to
	//declares the number of mines and flags
	private static final int SHADOWMINES = Color.CYAN;
	private static final int SHADOWFLAGS = Color.RED;
	
	//UI dependent members. 
	//Reinitialized in onCreate
	private Chronometer chronometer;
	private TableLayout table;
	private ImageButton resetButton;
	private Context ctx;
	private View result;
	private int textSize;
	private ToggleButton toggle;
	private TextView msgMines;
	private String resMsgMines;
	private Drawable mineDraw;
	private Drawable explosionDraw;
	private ZoomControls zoom;
	
	private double scale ;
	
	//UI independent members, saved across configuration changes
	private GameModel gameModel;
	
	//DB singleton instance 
	private DbHelper dbHelper;
	
	//Member values fetched from preferences and configuration files
	private int sizeBoard;
	private int numMines;
	private int bottonWidth;
	private int bottonHeight;
	
	private SharedPreferences sharedPreferences;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		
		//saves the member values across configuration changes 
		setRetainInstance(true);
		
		ctx = getActivity();
		dbHelper = DbHelper.getInstance(ctx);
		
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
		
		scale = (double)sharedPreferences.getFloat(SCALE_PREFERENCE, (float) DEFAULT_SCALE);
		
		numMines = sharedPreferences.getInt(StockPreferenceFragment.KEY_MINES_PREFERENCE, 
				StockPreferenceFragment.LEVEL_EASY_MINES);
		sizeBoard = sharedPreferences.getInt(StockPreferenceFragment.KEY_SIZE_PREFERENCE, 
				StockPreferenceFragment.LEVEL_EASY_SIZE);
		
		bottonWidth = getResources().getDimensionPixelSize(R.dimen.anchoBot);
		bottonHeight = getResources().getDimensionPixelSize(R.dimen.largoBot);
		textSize = (int)((bottonWidth/2)*scale);
		result = inflater.inflate(R.layout.main_fragment, container, false);		
		
		chronometer = (Chronometer) result.findViewById(R.id.chronometer);
		resetButton = (ImageButton) result.findViewById(R.id.resetButton);
		toggle = (ToggleButton) result.findViewById(R.id.toggleButton);
		msgMines = (TextView) result.findViewById(R.id.lblFlags);
		resMsgMines = getString(R.string.msgMines);
		msgMines.setText(String.format(resMsgMines, numMines, numMines));
		zoom = (ZoomControls) result.findViewById(R.id.zoomControls);
		mineDraw = getResources().getDrawable(R.drawable.mine);
		explosionDraw = getResources().getDrawable(R.drawable.explosion);
		//toggle button true means normal push sets a flag
		toggle.setChecked(sharedPreferences.getBoolean(FLAGS_PREFERENCE, false));
		toggle.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//the state of the toggle button is saved in a preference
				sharedPreferences.edit().putBoolean(FLAGS_PREFERENCE, toggle.isChecked()).apply();
			}
		});
		
		table = new TableLayout(ctx);
		//starts the asynchronously state load task
		if(null == gameModel){
			gameModel = new GameModel();
			dbHelper.loadModel(this, sizeBoard, numMines);
		//when a config change happens, the model it's supposed to be ready
		//so it just set game board
		}else if(gameModel.isReady()){
			setGame();
		}
		
		return result;
	}
	
	private void setGame() {
		resetButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				confirmReset();
			}
		});
		zoom.setOnZoomInClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(scale < UPPER_LIMIT ){
					scale += INCREMENT;
					updateScale();
				}
			}
		});
		zoom.setOnZoomOutClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(scale > BOTTOM_LIMIT){
					scale -= INCREMENT;
					updateScale();
				}
			}
			
		});
		//hides the progress bar and shows the board
		result.findViewById(R.id.progressBar).setVisibility(View.GONE);
		result.findViewById(R.id.boardContainer).setVisibility(View.VISIBLE);
		
		gameModel.clearDiscovered();
		
		//set the board with buttons
		setBoard();
		HorizontalScrollView boardLayout = (HorizontalScrollView) result.findViewById(R.id.boardLayout);
		boardLayout.addView(table);
		
		//restores the state of the game in case there is one
		//or is a configuration change
		if(gameModel.isGameStarted()){
			restoreState();
		}
	}
	//resets the board with the new scale, and stores the new value.
	private void updateScale(){
		sharedPreferences.edit().putFloat(SCALE_PREFERENCE, (float) scale).apply();
		textSize = (int)((bottonWidth*scale)/2);
		setBoard();
		gameModel.savedDiscovered.clear();
		restoreState();
	}

	protected void confirmReset() {
		if(gameModel.endedGame){
			resetState();
			setBoard();
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(R.string.confirmation)
			.setMessage(R.string.msg_reset)
			.setPositiveButton(R.string.okOpcion, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					resetState();
					setBoard();
				}
			})
			.setNegativeButton(R.string.cancelOpcion, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
		builder.create().show();
	}

	@Override
	public void onPause() {
		gameModel.lastChronometer = SystemClock.elapsedRealtime();
		gameModel.lastBase = chronometer.getBase();
		chronometer.stop();
		
		if(gameModel.startedGame)
			dbHelper.saveGameState(gameModel);
		super.onPause();
	}
	
	@Override
	public void onResume() {		
		continueGame();
		super.onResume();
	}
	
	private void continueGame() {
		if(gameModel.endedGame){
			chronometer.setBase(SystemClock.elapsedRealtime() - gameModel.chronometerFinal);
		}
		else{
			chronometer.setBase(gameModel.lastBase + 
					(SystemClock.elapsedRealtime() - gameModel.lastChronometer));
			chronometer.start();
		}
	}

	
	private void restoreState() {
		for(Integer id: gameModel.savedFlags){
			ImageButton button = (ImageButton) result.findViewById(id);
			button.setImageDrawable(new TextDrawable("?", textSize, SHADOWFLAGS));
		}
		for(Integer id: gameModel.savedPressed){
			discoverTile(id);
		}
	}
	
	@Override
	public void onDestroy() {
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
		super.onDestroy();
	}
	
    private void resetState(){
    	
    	gameModel.resetGame(sizeBoard, numMines);
    	dbHelper.resetGameState();
		
        chronometer.stop();
        gameModel.chronometerFinal  = 0;
        chronometer.setBase(SystemClock.elapsedRealtime());
        
        gameModel.endedGame = true;
        gameModel.startedGame = false;
    }
	//the board's id is generate incrementally
    //from 0 to size*size, it uses the raster scan order principle
    private void setBoard(){
    	
        table.removeAllViews();
        int k = 0;
        for(int i =0; i < sizeBoard; i++){
        	TableRow row =  new TableRow(ctx);
        	LayoutParams lParams = new LayoutParams(
        			LayoutParams.MATCH_PARENT,
        			LayoutParams.WRAP_CONTENT);
        	row.setLayoutParams(lParams);
        	for(int j = 0; j < sizeBoard; j++){
        		ImageButton tile = new ImageButton(ctx);
        		tile.setId(k++);
        		LayoutParams lpt = new LayoutParams(
        				(int)(bottonWidth*scale),
        				(int)(bottonHeight*scale));
        		tile.setLayoutParams(lpt);
        		tile.setOnClickListener(this);
        		tile.setOnLongClickListener(this);
        		row.addView(tile);
        	}
        	android.widget.TableLayout.LayoutParams lpr = new TableLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
        	table.addView(row, lpr);
        }
        updateMsgMines();
    }    
	//Recursively shows the tile and neighbors in case
    //it has no mines around
    private void showTile(int idPressed) {
    	if(gameModel.isTileValid(idPressed)){
    		gameModel.savedDiscovered.add(idPressed);
    		if(gameModel.savedFlags.contains(idPressed))
    			gameModel.savedFlags.remove(idPressed);

    		Set<Integer> neighbors = gameModel.getNeighbors(idPressed);
    		ImageButton button = (ImageButton) result.findViewById(idPressed);
    		button.setEnabled(false);
			int mines = gameModel.howManyMines(neighbors);
			if(mines == 0){
				button.setImageDrawable(null);
				discoverNeighbors(neighbors);
			}else{
				button.setImageDrawable(new TextDrawable(""+mines, textSize, SHADOWMINES));
			}
		}
	}

	private void discoverNeighbors(Set<Integer> neighbors) {
		for(Integer id: neighbors){
			showTile(id);
		}
	}

	private void discoverTile(int idPressed) {
		if(gameModel.hasMine(idPressed)){
			showAllMines();
			showExplosion(idPressed);
			chronometer.stop();
			if(!gameModel.endedGame){
				gameModel.chronometerFinal = (SystemClock.elapsedRealtime() - chronometer.getBase());
				gameModel.endedGame = true;
			}
			
		}
		else{
			showTile(idPressed);
			updateMsgMines();
			if(gameModel.isBoardClear()){
				showAllMines();
				chronometer.stop();
				if(!gameModel.endedGame){
					gameModel.chronometerFinal = (SystemClock.elapsedRealtime() - chronometer.getBase());
					mostrarMensaje();
					gameModel.endedGame = true;
				}				
			}
		}
	}
	//scales the mine's image and bind it to the button
	private void showExplosion(int idPressed) {
		ImageButton button = (ImageButton) result.findViewById(idPressed);
		int height = (int)(explosionDraw.getMinimumHeight() * (scale-0.3));
		int width = (int)(explosionDraw.getMinimumWidth() * (scale-0.3));			
		Bitmap bitmap = ((BitmapDrawable)explosionDraw).getBitmap();
		BitmapDrawable bd = new BitmapDrawable(
				getResources(), 
				Bitmap.createScaledBitmap(bitmap,width, height, true));
		button.setImageDrawable(bd);
	}

	
	private void showAllMines() {
		int height = (int)(mineDraw.getMinimumHeight() * (scale-0.3));
		int width = (int)(mineDraw.getMinimumWidth() * (scale-0.3));			
		Bitmap bitmap = ((BitmapDrawable)mineDraw).getBitmap();
		BitmapDrawable bd = new BitmapDrawable(
				getResources(), 
				Bitmap.createScaledBitmap(bitmap,width, height, true));
		
		for(Integer id: gameModel.minesLoc){
			ImageButton button = (ImageButton) result.findViewById(id);
			button.setImageDrawable(bd);
		}
	}   
    
    private void startGame(int id){
        gameModel.assignMines(id);
        
//        showAllMines();//DEBUG 
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        gameModel.endedGame = false;
        gameModel.startedGame = true;
    }

    
	//starts the dialog fragment to show the result of the game
	//and passes it the time, level, pond and a custom message
	private void mostrarMensaje() {
		final int seconds = (int) (gameModel.chronometerFinal / 1000);
		
		double pond = gameModel.getPond(seconds);
		PlayerDialog dialog = new PlayerDialog();
		Bundle extras = new Bundle();
		extras.putInt(PlayerDialog.TIME_ARGUMENT, seconds);
		
		int level = Integer.valueOf(sharedPreferences.getString(
				StockPreferenceFragment.KEY_DIFICULTY_PREFERENCE, 
				StockPreferenceFragment.LEVEL_EASY));
		extras.putInt(PlayerDialog.LEVEL_ARGUMENT, level);
		extras.putDouble(PlayerDialog.POND_ARGUMENT, pond);
		
		String msg_bd  = String.format("%sx%s",sizeBoard, numMines);
		extras.putString(PlayerDialog.SIZE_ARGUMENT, msg_bd);
		
		dialog.setArguments(extras);
		dialog.show(getFragmentManager(), PLAYER_DIALOG_FRAGMENT);
	}

	//either uncovers the tile pressed or sets a flag on it
	@Override
	public boolean onLongClick(View tile) {
		if(toggle.isChecked()){
			clickForMine(tile);
			return true;
		}
		if(gameModel.endedGame)return false;
		toggleFlag(tile);
		return true;
	}

	//either uncovers the tile pressed or sets a flag on it
	@Override
	public void onClick(View tile) {
		if(!toggle.isChecked()){
			clickForMine(tile);
		}else if(!gameModel.endedGame && gameModel.startedGame)
			toggleFlag(tile);
	}
	
	private void clickForMine(View tile){
		int idPressed = tile.getId();
		gameModel.savedPressed.add(idPressed);
		if(gameModel.endedGame && !gameModel.startedGame){
			startGame(idPressed);
			showTile(idPressed);
//			showAllMines();
		}else if(!gameModel.endedGame && gameModel.startedGame){
			discoverTile(idPressed);
		}
	}
	
	private void toggleFlag(View tile){
		ImageButton button = (ImageButton)tile;
		int idPressed = button.getId();
		
		if(gameModel.savedFlags.contains(idPressed)){
			button.setImageDrawable(null);
			gameModel.savedFlags.remove(idPressed);
		}else if(gameModel.flagsLeft()){
			button.setImageDrawable(new TextDrawable("?", textSize, SHADOWFLAGS));
			
			gameModel.savedFlags.add(idPressed);
		}
		updateMsgMines();
	}

	//updates the message under the toggle button
	private void updateMsgMines() {
		int mines = gameModel.MINES;
		msgMines.setText(String.format(resMsgMines, 
				(mines - gameModel.savedFlags.size()), mines));
	}

	//when a either the size of the number of mines changes via a preference
	//handler resets the game
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(StockPreferenceFragment.KEY_SIZE_PREFERENCE) ||
				key.equals(StockPreferenceFragment.KEY_MINES_PREFERENCE)) {
			this.numMines = sharedPreferences.getInt(StockPreferenceFragment.KEY_MINES_PREFERENCE, 
					StockPreferenceFragment.LEVEL_EASY_MINES);
			this.sizeBoard = sharedPreferences.getInt(StockPreferenceFragment.KEY_SIZE_PREFERENCE, 
					StockPreferenceFragment.LEVEL_EASY_SIZE);			
			resetState();
			gameModel.setReady(true);
			setBoard();
		}
	}

	//if it happens that a preference change occurs while it's still loading the game
	// state from DB, it should use the newly set values instead those from db
	@Override
	public void updateBoardUI(GameModel result) {
		if( !gameModel.isReady() )
			gameModel = result;
		setGame();
		continueGame();
	}
}
