package com.gmail.at.nowxue.minesweeper.model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
/*
 * Helper class to generate a drawable with text inside it.
 */
public class TextDrawable extends Drawable {

    private final String text;
    private final Paint paint;
    private int size;

    public TextDrawable(String text, int size, int shadowLayer) {

        this.text = text;
        this.size = size;
        this.paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(size);
        paint.setAntiAlias(true);
        paint.setFakeBoldText(true);
        paint.setShadowLayer(size/2, 0, 0, shadowLayer);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawText(text, size/2, size, paint);
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}