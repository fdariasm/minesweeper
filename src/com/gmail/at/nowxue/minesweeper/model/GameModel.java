package com.gmail.at.nowxue.minesweeper.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
/*
 * Game model class, stores the game's state at any moment. 
 * Eases the task of restoring the state after configuration changes or exiting
 * the application.
 * In the fragment, an instance of this object is "retain" so that the state is 
 * kept across configuration changes.
 * 
 * Member variables are store in the DB
 * 
 */

public class GameModel {
	//Stores the id of the mines in the board
	public Set<Integer> minesLoc = new HashSet<Integer>();
	//Used to find out when a mine is already discovered so that 
	//the app don't loop forever in state recovery.
	public Set<Integer> savedDiscovered = new HashSet<Integer>();
	//Save the id's of the tiles already discovered by the player
	public Set<Integer> savedPressed = new HashSet<Integer>();
	//Save the id's of the flags already set in the board
	public Set<Integer> savedFlags = new HashSet<Integer>();
	
	//Size of the board, it's a square SIZExSIZE
	public Integer SIZE;
	//The number of mines in the board, it depends on the level of configuration
	public Integer MINES;
	//Variables used to store the current time of the game
	// based on the android's time system...
	public long chronometerFinal = 0;
	public long lastChronometer = 0;
	public long lastBase = 0;
	
	public boolean endedGame = true;
	public boolean startedGame = false;
	
	//When it's loading from the DB, it's necessary to know when it's ready to restore state 
	private boolean ready = false;
	//The total number of slots minus the number of mines
	//Used to know when the player has cleared the board
	public int TOTALSLOTS;
	
	public void resetGame(int size, int mines){
		this.SIZE = size;
		this.MINES = mines;
		
		TOTALSLOTS = (this.SIZE * this.SIZE) - this.MINES;
		
		savedDiscovered.clear();
		minesLoc.clear();
		savedPressed.clear();
		savedFlags.clear();
	}
	
	public boolean isGameStarted(){
		return !savedPressed.isEmpty() || !minesLoc.isEmpty();
	}
	
	//Returns the id of the slot in the board given the row and column
	//based on raster scan order. A two dimensional array is store as a
	//single dimensional array.
	public int getIdBoard(int row, int col){
    	return SIZE * row + col;
    }
	
	
    public int getRoW(int id){
//    	android.util.Log.e("MyDebug", "id: " + id);
//    	android.util.Log.e("MyDebug", "row: " + ((int)id/SIZE));
    	return ((int)id / SIZE); 
    }
    
    public int getCol(int id){
//    	android.util.Log.e("MyDebug", "id: " + id);
//    	android.util.Log.e("MyDebug", "col: " + id%SIZE);
    	return id % SIZE;
    }
    //Randomly assigns the mines in the board from 0 to SIZE*SIZE
    //The assignation it's due when the user first presses a slot in the
    //board.
    public void assignMines(int idPressed){
    	Random rand = new Random(new Date().getTime());
    	int minePos;
    	//gets the neighbors of the slot pressed by the user
    	//we don't want any mine in that slots. Wouldn't be fun.
    	Set<Integer> neighbors = getNeighbors(idPressed);
    	
    	while(minesLoc.size() < MINES){
    		minePos = (rand.nextInt(SIZE * SIZE));
    		if(minePos != idPressed && !neighbors.contains(minePos)){
    			minesLoc.add(minePos);
    		}
    	}
    }
    //Generate a set of neighbor id's of the current pressed slot 
    public Set<Integer> getNeighbors(int id){
    	Set<Integer> neighbors = new HashSet<Integer>();
    	int col = getCol(id);
    	int fila = getRoW(id);
    	for(int i = fila-1; i < fila + 2; i++){
    		for(int j = col-1; j < col + 2; j++){
    			if(i< 0 || i >= SIZE || j < 0 || j >= SIZE)continue;
    			int idNeighbor = getIdBoard(i, j);
    			if(idNeighbor == id)continue;
    			neighbors.add(idNeighbor);
    		}
    	}
    	return neighbors;
    }

    public boolean isTileValid(int idPressed){
    	return !minesLoc.contains(idPressed) && !savedDiscovered.contains(idPressed);
    }
    
    public boolean hasMine(int idPressed){
    	return minesLoc.contains(idPressed);
    }
    
    public boolean isBoardClear(){
    	return savedDiscovered.size() == TOTALSLOTS;
    }
    
    public int howManyMines(Set<Integer> neighbors){
    	int cant = 0;
    	
    	for(Integer id: neighbors){
    		if(minesLoc.contains(id)){
    			cant ++;
    		}
    	}
    	return cant;
    }
    //a ponderation it's calculated based on the number of mines,
    //the time it took the player to clear the board, and the size 
    //o the board.
	public double getPond(int time){
		double cons = 10.0;
		double num = (double)MINES + 2*(double)SIZE;
		double den = Math.pow(SIZE, 2) / Math.pow(MINES, 2);
		return (num / den) + (cons / (double)time);
	}
    
    public void clearDiscovered(){
    	savedDiscovered.clear();
    }
	
    public boolean flagsLeft(){
    	return savedFlags.size() < MINES;
    }

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}
}
