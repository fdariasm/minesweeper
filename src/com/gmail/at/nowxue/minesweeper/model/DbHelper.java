package com.gmail.at.nowxue.minesweeper.model;


import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Build;
/*
 * Helper class to wrap the necessary access to the DB asynchronously.
 * Defines five tables. One for player score, and four with the last game's state
 * for future restoration.
 */

public class DbHelper extends SQLiteOpenHelper{
	
	public static final String DATABASE_NAME="players.db";
	private static final int SCHEMA=1;
	private static DbHelper singleton=null;
	
	public static final String PLAYERS_TABLE_NAME = "players";
	public static final String PLAYER = "player";
	public static final String TIME= "time";
	public static final String LEVEL = "level";
	public static final String LEVEL_SIZE = "level_size";
	public static final String LEVEL_POND= "level_pond";
	
	public static final String GAME_TABLE_NAME = "game_state";
	
	//Singleton strategy to avoid asynchronously conflicts
	public synchronized static DbHelper getInstance(Context ctxt) {
		if (singleton == null) {
			singleton = new DbHelper(ctxt.getApplicationContext());
		}
		return(singleton);
	}
	
	private DbHelper(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try{
			db.beginTransaction();
			db.execSQL("CREATE TABLE players (" +
					"_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"player TEXT, time INTEGER, level INTEGER, " +
					"level_size TEXT, level_pond REAL);");
			
			db.execSQL("CREATE TABLE game_state (" +
					"_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"size INTEGER, mines INTEGER, last_base INTEGER, chronometer_final INTEGER, " +
					"last_chronometer INTEGER, ended INTEGER, started INTEGER);"); //, mines_loc TEXT, pressed TEXT, flags TEXT
			
			db.execSQL("CREATE TABLE mines_loc (" +
					"id INTEGER PRIMARY KEY, " +
					"game_state INTEGER, FOREIGN KEY(game_state) " +
					"REFERENCES game_state(_id)" +
					");");
			db.execSQL("CREATE TABLE pressed (" +
					"id INTEGER PRIMARY KEY, " +
					"game_state INTEGER, FOREIGN KEY(game_state) " +
					"REFERENCES game_state(_id)" +
					");");
			db.execSQL("CREATE TABLE flags(" +
					"id INTEGER PRIMARY KEY, " +
					"game_state INTEGER, FOREIGN KEY(game_state) " +
					"REFERENCES game_state(_id)" +
					");");
			
			db.setTransactionSuccessful();
		}finally{
			db.endTransaction();
		}
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		throw new RuntimeException("How did we get here?");
	}
	
	public void saveScore(ContentValues values ){
		executeAsyncTask(new InsertTask(), values);
	}
	
	public void saveGameState(GameModel model){
		executeAsyncTask(new SaveStateTask(), model);
	}
	
	public void resetGameState(){
		executeAsyncTask(new ResetStateTask());
	}
	
	//Starts the model load from DB in a new task
	public void loadModel(OnGamestateListener listener, 
			Integer sizeBoard, Integer numMines){
		executeAsyncTask(new LoadGameStateTask(listener), sizeBoard, numMines);
	}

	@TargetApi(11)
	public static <T> void executeAsyncTask(AsyncTask<T, ?, ?> task,
	                                          T... params) {
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	      task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	    }
	    else {
	      task.execute(params);
	    }
	  }
	
	private class LoadGameStateTask extends AsyncTask<Integer, Void, GameModel>{
		private OnGamestateListener listener;
		
		public LoadGameStateTask(OnGamestateListener listener) {
			this.listener = listener;
		}
		//Loads the state from DB asynchronously and then calls the callback to
		//update the UI
		@Override
		protected GameModel doInBackground(Integer... params) {
//			SystemClock.sleep(10000); //Testing
			GameModel model = new GameModel();
			int sizeBoard = params[0], 
					numMines = params[1];
			
			SQLiteDatabase rdb = getReadableDatabase();
			Cursor gs = rdb.rawQuery("SELECT * FROM game_state;", null);
			
			gs.moveToFirst();
			if(gs.isAfterLast()){
				model.resetGame(sizeBoard, numMines);
				return model;
			}
			Integer id = gs.getInt(gs.getColumnIndex("_id"));
			sizeBoard = gs.getInt(gs.getColumnIndex("size"));
			numMines = gs.getInt(gs.getColumnIndex("mines"));
			model.resetGame(sizeBoard, numMines);
			model.lastBase = gs.getInt(gs.getColumnIndex("last_base"));
			model.chronometerFinal = gs.getInt(gs.getColumnIndex("chronometer_final"));
			model.lastChronometer = gs.getInt(gs.getColumnIndex("last_chronometer"));
			model.endedGame = gs.getInt(gs.getColumnIndex("ended")) > 0;
			model.startedGame = gs.getInt(gs.getColumnIndex("started")) > 0;
			gs = rdb.rawQuery("SELECT * FROM mines_loc where game_state = ?;", 
					new String[]{id.toString()});
			while(gs.moveToNext()){
				model.minesLoc.add(gs.getInt(gs.getColumnIndex("id")));
			}
			gs = rdb.rawQuery("SELECT * FROM flags where game_state = ?;", 
					new String[]{id.toString()});
			while(gs.moveToNext()){
				model.savedFlags.add(gs.getInt(gs.getColumnIndex("id")));
			}
			gs = rdb.rawQuery("SELECT * FROM pressed where game_state = ?;", 
					new String[]{id.toString()});
			while(gs.moveToNext()){
				model.savedPressed.add(gs.getInt(gs.getColumnIndex("id")));
			}
			
			return model;
		}
		
		@Override
		protected void onPostExecute(GameModel model) {
			model.setReady(true);
			listener.updateBoardUI(model);
		}
		
	}
	//saves the state with a transaction
	private class SaveStateTask extends AsyncTask<GameModel, Void, Void> {

		@Override
		protected Void doInBackground(GameModel... params){
			GameModel model = params[0];
			resetState();
			SQLiteDatabase wdb = getWritableDatabase();
			try{
				
				wdb.beginTransaction();

				ContentValues values = new ContentValues();

				values.put("size", model.SIZE);
				values.put("mines", model.MINES);
				values.put("chronometer_final", model.chronometerFinal);
				values.put("last_base", model.lastBase);
				values.put("last_chronometer", model.lastChronometer);
				values.put("started", model.startedGame?1:0);
				values.put("ended", model.endedGame?1:0);
				long id_inserted = wdb.insert(
						DbHelper.GAME_TABLE_NAME, null, values);

				for(int val:model.minesLoc){
					values = new ContentValues();
					values.put("id", val);
					values.put("game_state", id_inserted);
					wdb.insert("mines_loc", null, values);
				}

				for(int val:model.savedPressed){
					values = new ContentValues();
					values.put("id", val);
					values.put("game_state", id_inserted);
					wdb.insert("pressed", null, values);
				}

				for(int val:model.savedFlags){
					values = new ContentValues();
					values.put("id", val);
					values.put("game_state", id_inserted);
					wdb.insert("flags", null, values);
				}
				wdb.setTransactionSuccessful();
			}finally{
				wdb.endTransaction();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			android.util.Log.e("MyTag", "Complete save gameState");
		}
	}
	
	//There's always only one game state
	//so before the last state is saved, all the game states tables are
	//clean
	private void resetState(){
		SQLiteDatabase wdb = getWritableDatabase();
		try{
			wdb.beginTransaction();
			wdb.execSQL("DELETE FROM flags;");
			wdb.execSQL("DELETE FROM pressed;");
			wdb.execSQL("DELETE FROM mines_loc;");
			wdb.execSQL("DELETE FROM game_state;");
			wdb.setTransactionSuccessful();
		}finally{
			wdb.endTransaction();
		}
	}
	

	private class ResetStateTask extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground(Void... params) {
			
			resetState();
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			android.util.Log.e("Nowxue!", "reseted");
		}
	}
	
	private class InsertTask extends AsyncTask<ContentValues, Void, Void> {
		@Override
		protected Void doInBackground(ContentValues... params){
			getWritableDatabase().insert(
					DbHelper.PLAYERS_TABLE_NAME, null, params[0]);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			android.util.Log.e("MyTag", "Complete");
		}
	}
	
	public void showScores(OnResultListener listener, 
			String msg, int limit, int level){
		executeAsyncTask(new LoadCursorTask(listener, msg, limit, level));
	}
	
	//Interfaces to update UI in the main thread.
	public interface OnResultListener{
		public void showResults(Cursor result);
	}
	
	public interface OnGamestateListener{
		public void updateBoardUI(GameModel model);
	}
	
	private class LoadCursorTask extends AsyncTask<Void, Void, Void> {
		private Cursor resultCursor=null;
		private OnResultListener listener;
		private String lvl_msg;
		private int limit;
		private int level;
		
		public LoadCursorTask(OnResultListener listener, String msg, int limit, int level) {
			this.listener = listener;
			this.lvl_msg = msg;
			this.limit = limit;
			this.level = level;
		}
		/*
		 * (non-Javadoc)
		 * Instead using the DAO pattern or something similar,
		 * we follow the "normal" way in android by returning a cursor
		 * from the database with the results of the select. In the UI, mainly 
		 * list and grid views uses the cursor to show the results.
		 */
		@Override
		protected Void doInBackground(Void... arg0) {
			resultCursor =
					getReadableDatabase().rawQuery(String.format("SELECT _id, player, (time || ' Sec') as time, " +
							"'%s' as level, level_size, (level_pond * 100) as level_pond "
							+ "FROM players WHERE level = %s " +
							"ORDER BY level_pond DESC limit %s;", lvl_msg, level, limit),
							null);
			resultCursor.getCount();
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(null != resultCursor){
				listener.showResults(resultCursor);
			}
		}
	}
}
