package com.gmail.at.nowxue.minesweeper;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.gmail.at.nowxue.minesweeper.model.DbHelper;

/*
 * The main activity due to oversee most transactions 
 * in the application logic.
 */
public class MainActivity extends SherlockFragmentActivity 
					implements FragmentManager.OnBackStackChangedListener,
					PlayerDialog.NoticeDialogListener{
	
	private View sidebar;
	private View divider;
	
	private SimpleContentFragment help=null;
	private SimpleContentFragment about=null;

	private static final String HELP = "help";
	private static final String ABOUT = "about";
	private static final String FILE_HELP=
			"file:///android_asset/misc/help.html";
	private static final String FILE_ABOUT=
			"file:///android_asset/misc/about.html";
	
	private GameBoard gameBoard;

	private DbHelper dbAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		gameBoard = (GameBoard) getSupportFragmentManager().findFragmentById(R.id.mainframe);
		if (gameBoard == null) {
			gameBoard = new GameBoard();
			getSupportFragmentManager().beginTransaction()
			.add(R.id.mainframe,
					gameBoard).commit();
		    }
		
		help=(SimpleContentFragment)getSupportFragmentManager().findFragmentByTag(HELP);
		about=(SimpleContentFragment)getSupportFragmentManager().findFragmentByTag(ABOUT);
		
		sidebar = findViewById(R.id.sidebar);
		divider = findViewById(R.id.divider);
		getSupportFragmentManager().addOnBackStackChangedListener(this);
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			openSidebar();
		}
		dbAdapter = DbHelper.getInstance(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.options, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			return true;
		case R.id.about:
			showAbout();
			return true;
		case R.id.help:
			showHelp();
			return true;
		case R.id.settings:
			startActivity(new Intent(this, Preferences.class));
			return true;
		case R.id.scores:
			startActivity(new Intent(this, ScoresList.class));
		}
		return super.onOptionsItemSelected(item);
	}

	void openSidebar() {
		LinearLayout.LayoutParams p=
				(LinearLayout.LayoutParams)sidebar.getLayoutParams();
		if (p.weight == 0) {
			p.weight=3;
			sidebar.setLayoutParams(p);
		}
		divider.setVisibility(View.VISIBLE);
	}
	
	void showAbout() {
		if (sidebar != null){
			openSidebar();
			if (about==null) {
				about=SimpleContentFragment.newInstance(FILE_ABOUT);
			}
			getSupportFragmentManager().beginTransaction()
			.addToBackStack(null)
			.replace(R.id.sidebar, about).commit();
		}else {
			Intent
			i=new Intent(this, SimpleContentActivity.class);
			i.putExtra(SimpleContentActivity.EXTRA_FILE, FILE_ABOUT);
			startActivity(i);
		}
	}

	void showHelp() {
		if (sidebar != null){
			openSidebar();
			if (help==null) {
				help=SimpleContentFragment.newInstance(FILE_HELP);
			}
			getSupportFragmentManager().beginTransaction()
			.addToBackStack(null)
			.replace(R.id.sidebar, help).commit();
		}
		else {
			Intent
			i=new
			Intent(this, SimpleContentActivity.class);
			i.putExtra(SimpleContentActivity.EXTRA_FILE, FILE_HELP);
			startActivity(i);
		}
	}

	@Override
	public void onBackStackChanged() {
		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			LinearLayout.LayoutParams p=
					(LinearLayout.LayoutParams)sidebar.getLayoutParams();
			if (p.weight > 0) { 
				p.weight=0;
				sidebar.setLayoutParams(p);
				divider.setVisibility(View.GONE);
			}
		}
	}
	/*
	 * The activity must implement some interfaces in order to get a callback
	 * from the fragments that it uses.
	 */
	@Override
	public void onDialogPositiveClick(int userTime, String playerName, 
			int level, double pond, String lvl_size) {
		
		if(null == playerName || playerName.isEmpty()) return;
		ContentValues values = new ContentValues();
		values.put(DbHelper.LEVEL, level);
		values.put(DbHelper.PLAYER, playerName);
		values.put(DbHelper.TIME, userTime);
		values.put(DbHelper.LEVEL_POND, pond);
		values.put(DbHelper.LEVEL_SIZE, lvl_size);
		
		dbAdapter.saveScore(values);
	}
	

	@Override
	public void onDialogNegativeClick(SherlockDialogFragment dialog) {		
	}
	
}
