package com.gmail.at.nowxue.minesweeper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.gmail.at.nowxue.minesweeper.model.DbHelper;

//Dialog fragment to show the time and ask the user a name
//to store in scores
public class PlayerDialog extends SherlockDialogFragment {
	
	public interface NoticeDialogListener{
		public void onDialogPositiveClick(int userTime, String playerName, int level, 
				double pond, String lvl_size);
        public void onDialogNegativeClick(SherlockDialogFragment dialog);
	}
	
	public static final String TIME_ARGUMENT = DbHelper.TIME;
	public static final String LEVEL_ARGUMENT = DbHelper.LEVEL;
	public static final String POND_ARGUMENT = DbHelper.LEVEL_POND;
	public static final String SIZE_ARGUMENT = DbHelper.LEVEL_SIZE;
	
	private NoticeDialogListener mListener;
	private int userTime;
	private String msg_level_size;
	private int level;
	private double level_pond;
	private EditText playerName;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		Bundle arguments = getArguments();
		
		userTime = arguments.getInt(TIME_ARGUMENT);
		msg_level_size = arguments.getString(DbHelper.LEVEL_SIZE);
		level = arguments.getInt(LEVEL_ARGUMENT);
		level_pond = arguments.getDouble(DbHelper.LEVEL_POND);
		//inflate a custom layout to show in the dialog
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View inflate = inflater.inflate(R.layout.player_layout, null);
		TextView viewMensaje = (TextView)inflate.findViewById(R.id.viewMensaje);
		viewMensaje.setText(String.format(
				getString(R.string.mensajeTiempo), userTime));
		playerName = (EditText)inflate.findViewById(R.id.playername);
		
		builder.setView(inflate)
			.setTitle(getString(R.string.tituloDialog))
			.setPositiveButton(R.string.okOpcion, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onDialogPositiveClick(userTime, 
							playerName.getText().toString(), level, level_pond, msg_level_size);
					
				}
			})
			.setNegativeButton(R.string.cancelOpcion, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onDialogNegativeClick(PlayerDialog.this);
				}
			});
					
		return builder.create();
	}
	
}
