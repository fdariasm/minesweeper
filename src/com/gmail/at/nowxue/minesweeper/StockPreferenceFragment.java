package com.gmail.at.nowxue.minesweeper;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;

import com.michaelnovakjr.numberpicker.NumberPickerPreference;

//Handles the different preferences of the application
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class StockPreferenceFragment extends 
						PreferenceFragment implements OnSharedPreferenceChangeListener{
	
	public static final String KEY_DIFICULTY_PREFERENCE = "difficulty";
	public static final String KEY_SIZE_PREFERENCE = "level_size";
	public static final String KEY_MINES_PREFERENCE = "level_mines";
	public static final String KEY_LIST_PREFERENCE = "score_list";
	
	public static final String LEVEL_EASY = "0";
	public static final String LEVEL_MEDIUM = "1";
	public static final String LEVEL_CUSTOM = "2";
	
	public static final int TOP_DEFAULT = 20;
	
	public static final int LEVEL_EASY_SIZE = 8;
	public static final int LEVEL_EASY_MINES= 10;
	public static final int LEVEL_MEDIUM_SIZE = 16;
	public static final int LEVEL_MEDIUM_MINES= 40;
	
	private ListPreference mListPreference;
	private NumberPickerPreference mNumberPickerSize;
	private NumberPickerPreference mNumberPickerMines;
	private NumberPickerPreference mTopScorePicker;
	private SharedPreferences sharedPreferences;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		int res = getActivity()
				.getResources()
				.getIdentifier(getArguments()
						.getString("resource"), "xml", getActivity().getPackageName());
		addPreferencesFromResource(res);
		
		mListPreference = (ListPreference) findPreference(KEY_DIFICULTY_PREFERENCE);
		mNumberPickerSize = (NumberPickerPreference) findPreference(KEY_SIZE_PREFERENCE);
		mNumberPickerMines= (NumberPickerPreference) findPreference(KEY_MINES_PREFERENCE);
		mTopScorePicker = (NumberPickerPreference) findPreference(KEY_LIST_PREFERENCE);
		
		sharedPreferences = getPreferenceManager().getSharedPreferences();
	}

	//We listen a preference change so that we can update the summary
	//and activate or deactivate some controls
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		//disable the mines and size controls by default
		if (key.equals(KEY_DIFICULTY_PREFERENCE)) {
			mNumberPickerMines.setEnabled(false);
        	mNumberPickerSize.setEnabled(false);
            mListPreference.setSummary(String.format(
            		getActivity().getString(R.string.level_msg), mListPreference.getEntry().toString()));
            String value = mListPreference.getValue();
            if(value.equals(LEVEL_EASY)){
            	Editor editor = sharedPreferences.edit();
            	editor.putInt(KEY_SIZE_PREFERENCE, LEVEL_EASY_SIZE);
            	editor.putInt(KEY_MINES_PREFERENCE, LEVEL_EASY_MINES);
            	editor.apply();
            }else if(value.equals(LEVEL_MEDIUM)){
            	Editor editor = sharedPreferences.edit();
            	editor.putInt(KEY_SIZE_PREFERENCE, LEVEL_MEDIUM_SIZE);
            	editor.putInt(KEY_MINES_PREFERENCE, LEVEL_MEDIUM_MINES);
            	editor.apply();
            }else if(value.equals(LEVEL_CUSTOM)){
            	//just if a custom is selected, activate the mines and size controls
            	mNumberPickerMines.setEnabled(true);
            	mNumberPickerSize.setEnabled(true);
            }
        }else if(key.equals(KEY_MINES_PREFERENCE) || key.equals(KEY_SIZE_PREFERENCE)){
        	mNumberPickerSize.setSummary(String.format(
    				getActivity().getString(R.string.sizePicker), 
    				sharedPreferences.getInt(KEY_SIZE_PREFERENCE, LEVEL_EASY_SIZE)));
        	mNumberPickerMines.setSummary(String.format(
    				getActivity().getString(R.string.minePicker), 
    				sharedPreferences.getInt(KEY_MINES_PREFERENCE, LEVEL_EASY_MINES)));
        }else{
        	mTopScorePicker.setSummary(String.format(
        			getActivity().getString(R.string.scoreListPrefSum), 
        			sharedPreferences.getInt(KEY_LIST_PREFERENCE, TOP_DEFAULT)));
        }
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(!LEVEL_CUSTOM.equals(mListPreference.getValue())){
			mNumberPickerMines.setEnabled(false);
        	mNumberPickerSize.setEnabled(false);
		}
		
		mListPreference.setSummary(String.format(
        		getActivity().getString(R.string.level_msg), mListPreference.getEntry().toString()));
		
		
		mNumberPickerSize.setSummary(String.format(
				getActivity().getString(R.string.sizePicker), 
				sharedPreferences.getInt(KEY_SIZE_PREFERENCE, LEVEL_EASY_SIZE)));
		
		mNumberPickerMines.setSummary(String.format(
				getActivity().getString(R.string.minePicker), 
				sharedPreferences.getInt(KEY_MINES_PREFERENCE, LEVEL_EASY_MINES)));
		
		mTopScorePicker.setSummary(String.format(
    			getActivity().getString(R.string.scoreListPrefSum), 
    			sharedPreferences.getInt(KEY_LIST_PREFERENCE, TOP_DEFAULT)));
		//register this as a listener of the preferences
		 getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);    

	}
	
}
