package com.gmail.at.nowxue.minesweeper;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.SimpleCursorAdapter;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.gmail.at.nowxue.minesweeper.model.DbHelper;
/*
 * List activity to show the scores as a list of results
 * depending on the current level
 */
public class ScoresList extends SherlockListActivity
						implements DbHelper.OnResultListener{
	
	private static final String LEVEL_STATE = "level_state";
	private DbHelper dbAdapter;
	private SharedPreferences sharedPreferences;
	private int level;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.score_layout);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		level = Integer.valueOf(sharedPreferences.getString(
				StockPreferenceFragment.KEY_DIFICULTY_PREFERENCE, 
				StockPreferenceFragment.LEVEL_EASY));
		
		if(null != savedInstanceState){
			Integer lvl = savedInstanceState.getInt(LEVEL_STATE);
			if(null != lvl)
				level = lvl;
		}
			

		dbAdapter = DbHelper.getInstance(this);
		showScore();
	}
	//menu to change the kind of results showed in the activity
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.score_options, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.easyScore:
			level = 0;
			showScore();
			return true;
		case R.id.mediumScore:
			level = 1;
			showScore();
			return true;
		case R.id.customScore:
			level = 2;
			showScore();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//a configuration change could occur, then we save the current level on a bundle
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(LEVEL_STATE, level);
		super.onSaveInstanceState(outState);
	}
	
	//Asynchronously loads the results for the givel level
	private void showScore(){
		String lvl_msg = getResources().getStringArray(R.array.difficultyArray)[level];
		//it's possible to customize the number of top results showed by the activity
		int limit = sharedPreferences.getInt(StockPreferenceFragment.KEY_LIST_PREFERENCE, 
				StockPreferenceFragment.TOP_DEFAULT);
		dbAdapter.showScores(this, lvl_msg, limit, level);
	}
	//callback to actually show the results
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressWarnings("deprecation")
	@Override
	public void showResults(Cursor result) {
		SimpleCursorAdapter adapter;
		String[] columns = {DbHelper.PLAYER, DbHelper.LEVEL, DbHelper.LEVEL_SIZE, DbHelper.TIME, DbHelper.LEVEL_POND};
		int[] resources = {R.id.playerView, R.id.levelView, R.id.sizeView, R.id.timeView, R.id.scoreView};
		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			adapter = new SimpleCursorAdapter(ScoresList.this, R.layout.row2, 
					result, columns, resources, 0);
		}else{
			adapter = new SimpleCursorAdapter(ScoresList.this, R.layout.row2, 
					result, columns, resources);
		}
		
		setListAdapter(adapter);
	}	
}
